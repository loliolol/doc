package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class doc_4 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner comScanner = new Scanner(new File("Point.java"));
        String Class = "Null"; String Deprecateon = ""; String Author = "Null"; String Version = "Null"; String Since = "Null";

        while (comScanner.hasNextLine()) {
            String str = comScanner.nextLine();
            if (str.contains("*") && !(str.contains("/**") || str.contains("*/"))) {
                String[] mas = str.replace("*", "").trim().split(" ", 2);
                switch (mas[0].toLowerCase()) {
                    case "@author" -> Author = mas[1];
                    case "@version" -> Version = mas[1];
                    case "@since" -> Since = mas[1];
                    default -> Deprecateon = str.replace("*", "").trim();
                }
            }
            if (str.toLowerCase().contains("class")){
                Class = str.replace("{", "").trim();
                break;
            }
        }
        String table_1 = "<table cellpadding=\"7\" align=\"center\" bgcolor=\"#f75797\" >" +
                "<tr>" +
                "<td>Class</td>" +
                "<td>Deprecateon</td>" +
                "<td>Author</td>" +
                "<td>Version</td>" +
                "<td>Since</td>" +
                "</tr>" +
                "<tr>" +
                "<td>" + Class + "</td>" +
                "<td>" + Deprecateon + "</td>" +
                "<td>" + Author + "</td>" +
                "<td>" + Version + "</td>" +
                "<td>" + Since + "</td>" +
                "</tr>" +
                "</table>";

        String table_2 = ""; String signature = "Null"; Deprecateon = ""; String Param = "Null"; String Return = "Null";

        while (comScanner.hasNextLine()) {
            String str = comScanner.nextLine();
            if (str.contains("*") && !(str.contains("/**") || str.contains("*/"))) {
                String[] mas = str.replace("*", "").trim().split(" ", 2);
                switch (mas[0].toLowerCase()) {
                    case "@return" -> Return = mas[1];
                    case "@param" ->  Param = Param.replace("Null", "") + "<br>" +  mas[1];
                    default -> Deprecateon += str.replace("*", "").trim();
                }
            }
            if (str.toLowerCase().contains("*/")){ // если нашли символ последнего коментария
                while (comScanner.hasNextLine()) {
                    String sign_not = comScanner.nextLine();
                    if(!sign_not.trim().equals("")){ // смотрем если строка не пустая
                        signature = sign_not.replace("{", "").replace(";", "").trim();
                        table_2 += "<tr>" +
                                "<td>" + signature + "</td>" +
                                "<td>" + Deprecateon + "</td>" +
                                "<td>" + Param + "</td>" +
                                "<td>" + Return + "</td>" +
                                "</tr>";
                        // обновляем переменные
                        signature = "Null"; Deprecateon = ""; Param = "Null"; Return = "Null";
                        break;
                    }
                }
            }
        }
        table_2 = "<table bgcolor=\"#e0b46c\" align=\"center\" cellpadding=\"7\">" +
                "<tr bgcolor=\"#bd7f2a\">" +
                "<td>signature</td>" +
                "<td>Deprecateon</td>" +
                "<td>Param</td>" +
                "<td>Return</td>" +
                "</tr>" +
                table_2 +
                "</table>";

        String HTMl_title = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n" +
                "<html>\n" +
                "<head>\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
                "    <title>Заголовок страницы для браузера</title>\n" +
                "</head>\n" +
                "<body>" +
                table_1 +
                "<p><br><p>" +
                table_2 +
                "</body>" +
                "</html>";

        try(FileWriter writer = new FileWriter("test_2.html", false))
        {
            writer.write(HTMl_title);
            writer.flush();
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }

    }
}
